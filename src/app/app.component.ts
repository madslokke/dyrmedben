import { Component } from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {FirestoreService} from 'extended-angular-firebase';
import {NewAnimalService} from './services/new-animal.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  dataArray: any[] = [];
  displayedColumns = ['name', 'legs', 'edit'];
  dataSource: any;

  constructor(
    db: FirestoreService,
    public newAnimal: NewAnimalService
  ) {
    db.collection('animals').subscribe(data => {
      this.dataArray = [];
      for (let i = 0; i < data.length; i++) {
        this.dataArray.push(data[i].data);
      }
      console.log(data);
      this.dataSource = new MatTableDataSource(this.dataArray);
      console.log(this.dataSource);
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  addAnimal() {

  }
}
