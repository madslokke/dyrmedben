import { Injectable } from '@angular/core';
import {MatDialog} from '@angular/material';
import {AddNewDialogComponent} from '../components/add-new-dialog/add-new-dialog.component';

@Injectable()
export class NewAnimalService {

  constructor(
    private dialog: MatDialog
  ) { }

  openDialog(data = null, path: string = '') {

    if (!data) {
      data = {};
    }

    if (path) {
      data['path'] = path;
    }

    this.dialog.open(AddNewDialogComponent, {
      width: '400px',
      data: data
    });
  }
}
