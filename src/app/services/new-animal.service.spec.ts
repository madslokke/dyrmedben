import { TestBed, inject } from '@angular/core/testing';

import { NewAnimalService } from './new-animal.service';

describe('NewAnimalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NewAnimalService]
    });
  });

  it('should be created', inject([NewAnimalService], (service: NewAnimalService) => {
    expect(service).toBeTruthy();
  }));
});
