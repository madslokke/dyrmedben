import {Component, Inject, OnInit} from '@angular/core';
import {AngularFirestore, AngularFirestoreDocument} from 'angularfire2/firestore';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FirestoreService} from 'extended-angular-firebase';
import * as firebase from 'firebase/app';
import DocumentReference = firebase.firestore.DocumentReference;

@Component({
  selector: 'app-add-new-dialog',
  templateUrl: './add-new-dialog.component.html',
  styleUrls: ['./add-new-dialog.component.css']
})
export class AddNewDialogComponent implements OnInit {

  disabled;

  constructor(
    private firestore: AngularFirestore,
    public dialogRef: MatDialogRef<AddNewDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.disabled = false;
  }

  public async newAnimal() {
    if (this.data.name && this.data.legs && !this.disabled ) {
      this.disabled = true;
      let res;

      if (this.data.path) {
        const ref = this.firestore.doc(this.data.path);

        delete this.data.path;
        res = await ref.update(this.data);
      } else {
        delete this.data.path;
        res = await this.firestore.collection('animals').add(this.data);
      }

      this.dialogRef.close(this.data);
      this.disabled = false;
    }
  }


  public deleteAnimal() {
    if (this.data.path) {
      this.dialogRef.close(this.data);
      this.firestore.doc(this.data.path).delete();
    }
  }

}
