import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatDialog,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatTableModule
} from '@angular/material';

import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {environment} from '../environments/environment';
import {AngularFireModule} from 'angularfire2';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import {ExtendedFirestoreModule} from 'extended-angular-firebase';
import { AddNewDialogComponent } from './components/add-new-dialog/add-new-dialog.component';
import {FormsModule} from '@angular/forms';
import {NewAnimalService} from './services/new-animal.service';


@NgModule({
  entryComponents: [
    AddNewDialogComponent,
  ],
  declarations: [
    AppComponent,
    AddNewDialogComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    MatButtonModule,
    MatFormFieldModule,
    MatTableModule,
    MatInputModule,
    MatIconModule,
    MatDialogModule,
    FormsModule,
    BrowserAnimationsModule,
    ExtendedFirestoreModule
  ],
  providers: [
    NewAnimalService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
