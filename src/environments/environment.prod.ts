export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyALRKU_7ZXARygrlP5Z8XUWdSMv6TOtcTQ',
    authDomain: '<your-project-authdomain>',
    databaseURL: '<your-database-URL>',
    projectId: 'dyr-med-ben',
    storageBucket: '<your-storage-bucket>',
    messagingSenderId: '<your-messaging-sender-id>'
  }
};